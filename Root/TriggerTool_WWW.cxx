#include "CxAODTools_WZ/TriggerTool_WWW.h"

#include "CxAODTools_WZ/CommonProperties_WWW.h"

TriggerTool_WZ::TriggerTool_WZ(ConfigStore& config) : TriggerTool(config) {}

EL::StatusCode TriggerTool_WZ::initTools() {
  EL_CHECK("TriggerTool_WZ::initTools()", initMuonSFTool("Medium"));
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TriggerTool_WZ::initProperties() {
  m_electronEffProp = &Props::trigEFFtightLHIsoFixedCutLoose;
  m_electronSFProp = &Props::trigSFtightLHIsoFixedCutLoose;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TriggerTool_WZ::initTriggers() {
  addLowestUnprescaledElectron_WZ();
  addLowestUnprescaledMuon_WZ();
  return EL::StatusCode::SUCCESS;
}

void TriggerTool_WZ::addLowestUnprescaledElectron_WZ() {

  ADD_TRIG_MATCH(HLT_e24_lhmedium_L1EM20VH, any,  data15, data15);
  ADD_TRIG_MATCH(HLT_e60_lhmedium,          any,  data15, data15);
  ADD_TRIG_MATCH(HLT_e120_lhloose,          any,  data15, data15);
        
  ADD_TRIG_MATCH(HLT_e26_lhtight_nod0_ivarloose, any,  data16A, data18);
  ADD_TRIG_MATCH(HLT_e60_lhmedium_nod0,          any,  data16A, data18);
  ADD_TRIG_MATCH(HLT_e140_lhloose_nod0,          any,  data16A, data18);
                  
}

void TriggerTool_WZ::addLowestUnprescaledMuon_WZ() {

  ADD_TRIG_MATCH(HLT_mu20_iloose_L1MU15,    any,  data15, data15);
  ADD_TRIG_MATCH(HLT_mu50,                  any,  data15, data18);
  ADD_TRIG_MATCH(HLT_mu26_ivarmedium,       any,  data16A, data18);

}
