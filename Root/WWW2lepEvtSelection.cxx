#include "CxAODTools_WZ/WWW2lepEvtSelection.h"
#include "CxAODTools/ReturnCheck.h"

#include "xAODEgamma/Electron.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/Jet.h"
#include "xAODMuon/Muon.h"

#include "TLorentzVector.h"

#include <iostream>

void WWW2lepEvtSelection::clearResult() {
  m_result.pass = false;
  m_result.signalJets.clear();
  m_result.taus.clear();
  m_result.el1 = nullptr;
  m_result.el2 = nullptr;
  m_result.el3 = nullptr;
  m_result.mu1 = nullptr;
  m_result.mu2 = nullptr;
  m_result.mu3 = nullptr;
  m_result.met = nullptr;
}

bool WWW2lepEvtSelection::passSelection(SelectionContainers& containers, bool isKinVar) {
  // here just as an example:
  // if a new passKinematics() function is defined with some variables in the prototype,
  // one needs to reimplement passSelection here
  // otherwise, don't need to put any code
  return WWWEvtSelection<ResultWWW2lep>::passSelection(containers, isKinVar);
}

bool WWW2lepEvtSelection::passLeptonSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons,
                                              const xAOD::MissingET* met) {
  int res = doLeptonSelection(electrons, muons, m_result.el1, m_result.el2, m_result.el3, m_result.mu1, m_result.mu2, m_result.mu3);
  if (res != 2) {
    return false;
  }
  m_result.met = met;
  return true;
}

bool WWW2lepEvtSelection::passKinematics() {
  // MJ cuts, like MET / MPT etc...
  // my advice is to add in passKinematics() prototype all the stuff that
  // doesn't need to be put in the Result struct, like MPT

  return true;
}

//
// Pre-selection versions
bool WWW2lepEvtSelection::passPreSelection(SelectionContainers& containers, bool isKinVar) {
  // here just as an example:
  // if a new passKinematics() function is defined with some variables in the prototype,
  // one needs to reimplement passSelection here
  // otherwise, don't need to put any code
  bool passpreselection = WWWEvtSelection<ResultWWW2lep>::passPreSelection(containers, isKinVar);

  // if (passpreselection && !isKinVar) {
  //   TLorentzVector lep1(0, 0, 0, 0);
  //   TLorentzVector lep2(0, 0, 0, 0);
  //   if (m_result.mu1 != 0) {
  //     lep1.SetPtEtaPhiE(m_result.mu1->pt(), m_result.mu1->eta(), m_result.mu1->phi(), m_result.mu1->e());
  //   }
  //   if (m_result.mu2 != 0) {
  //     if (lep1.Mag() == 0) {
  //       lep1.SetPtEtaPhiE(m_result.mu2->pt(), m_result.mu2->eta(), m_result.mu2->phi(), m_result.mu2->e());
  //     } else {
  //       lep2.SetPtEtaPhiE(m_result.mu2->pt(), m_result.mu2->eta(), m_result.mu2->phi(), m_result.mu2->e());
  //     }
  //   }

  //   if (m_result.el1 != 0) {
  //     if (lep1.Mag() == 0) {
  //       lep1.SetPtEtaPhiE(m_result.el1->pt(), m_result.el1->eta(), m_result.el1->phi(), m_result.el1->e());
  //     } else {
  //       lep2.SetPtEtaPhiE(m_result.el1->pt(), m_result.el1->eta(), m_result.el1->phi(), m_result.el1->e());
  //     }
  //   }
  //   if (m_result.el2 != 0) {
  //     if (lep1.Mag() == 0) {
  //       lep1.SetPtEtaPhiE(m_result.el2->pt(), m_result.el2->eta(), m_result.el2->phi(), m_result.el2->e());
  //     } else {
  //       lep2.SetPtEtaPhiE(m_result.el2->pt(), m_result.el2->eta(), m_result.el2->phi(), m_result.el2->e());
  //     }
  //   }
  //   TLorentzVector dilep = lep1 + lep2;
  //   double PtValue = dilep.Pt();
  //   if (PtValue / 1000 <= 90) {
  //     m_cutFlow.count("Pt < 90GeV", 200);
  //   }
  //   if (PtValue / 1000 > 90 && PtValue / 1000 <= 120) {
  //     m_cutFlow.count("90GeV < Pt < 120GeV", 201);
  //   }
  //   if (PtValue / 1000 > 120 && PtValue / 1000 <= 160) {
  //     m_cutFlow.count("120GeV < Pt < 160GeV", 202);
  //   }
  //   if (PtValue / 1000 > 160 && PtValue / 1000 <= 200) {
  //     m_cutFlow.count("160GeV < Pt < 200GeV", 203);
  //   }
  //   if (PtValue / 1000 > 200) {
  //     m_cutFlow.count("Pt > 200GeV", 204);
  //   }
  // }
  return passpreselection;
}

bool WWW2lepEvtSelection::passLeptonPreSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons,
                                                 const xAOD::MissingET* met) {
  int res = doLeptonPreSelection(electrons, muons, m_result.el1, m_result.el2, m_result.el3, m_result.mu1, m_result.mu2, m_result.mu3);
  if (res != 2) {
    return false;
  }
  m_result.met = met;
  return true;
}

EL::StatusCode WWW2lepEvtSelection::writeEventVariables(const xAOD::EventInfo* /*eventInfoIn*/, xAOD::EventInfo* /*eventInfoOut*/,
                                                        bool /*isKinVar*/, bool /*isWeightVar*/, std::string /*sysName*/,
                                                        int /*rdm_RunNumber*/, CP::MuonTriggerScaleFactors* /*trig_sfmuon*/) {
  // This part is no longer used. It is the remaining piece from CxAOD framework

  // bool sysForLeptonSF=!(TString(sysName).BeginsWith("JET"));

  // // ensure to write all variables for all events
  // if(sysForLeptonSF) {
  //   Props::leptonSF.set(eventInfoOut, 1);
  // }

  // if (!m_result.pass) return EL::StatusCode::SUCCESS;

  // // lepton SF
  // // Not used?
  // float leptonSF = 1;
  // if (m_result.type() == ResultWWW2lep::Type::ee) {
  //   leptonSF *= Props::effSFlooseLH.get(m_result.el1)*
  //     Props::effSFReco.get(m_result.el1)*
  //     Props::effSFIsoFixedCutLooseLooseLH.get(m_result.el1);
  //   leptonSF *= Props::effSFlooseLH.get(m_result.el2)*
  //     Props::effSFReco.get(m_result.el2)*
  //     Props::effSFIsoFixedCutLooseLooseLH.get(m_result.el2);
  // } else if (m_result.type() == ResultWWW2lep::Type::mm) {
  //   leptonSF *= Props::effSF.get(m_result.mu1)*
  //     Props::TTVAEffSF.get(m_result.mu1)*
  //     Props::looseTrackOnlyIsoSF.get(m_result.mu1);
  //   leptonSF *= Props::effSF.get(m_result.mu2)*
  //     Props::TTVAEffSF.get(m_result.mu2)*
  //     Props::looseTrackOnlyIsoSF.get(m_result.mu2);
  // } else if (m_result.type() == ResultWWW2lep::Type::em) {
  //   leptonSF *= Props::effSFlooseLH.get(m_result.el1)*
  //     Props::effSFReco.get(m_result.el1)*
  //     Props::effSFIsoFixedCutLooseLooseLH.get(m_result.el1);
  //   leptonSF *= Props::effSF.get(m_result.mu1)*
  //     Props::TTVAEffSF.get(m_result.mu1)*
  //     Props::looseTrackOnlyIsoSF.get(m_result.mu1);
  // }

  // if(sysForLeptonSF) {
  //   Props::leptonSF.set(eventInfoOut, leptonSF);
  // }

  return EL::StatusCode::SUCCESS;
}
