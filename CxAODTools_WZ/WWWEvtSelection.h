#ifndef CxAODTools__WWWEvtSelection_H
#define CxAODTools__WWWEvtSelection_H

#include "CxAODTools/EventSelection.h"

namespace xAOD {
#ifndef XAODEGAMMA_ELECTRON_H
class Electron;
#endif
#ifndef XAODEGAMMA_PHOTON_H
class Photon;
#endif
#ifndef XAODMUON_MUON_H
class Muon;
#endif
#ifndef XAODTAU_TAUJET_H
class TauJet;
#endif
#ifndef XAODJET_JET_H
class Jet;
#endif
}  // namespace xAOD

template <typename T>
class WWWEvtSelection : public EventSelection {
 public:
  WWWEvtSelection() noexcept;
  virtual ~WWWEvtSelection() noexcept {}

  virtual T& result() { return m_result; }  // probably should be const ?

  virtual bool passPreSelection(SelectionContainers& containers, bool isKinVar) override;
  virtual bool passSelection(SelectionContainers& containers, bool isKinVar) override;

 protected:
  T m_result;
  virtual void clearResult() = 0;

  virtual bool passJetPreSelection(const xAOD::JetContainer* jets);
  virtual bool passJetSelection(const xAOD::JetContainer* jets);

  virtual bool passTauPreSelection(const xAOD::TauJetContainer* taus);
  virtual bool passTauSelection(const xAOD::TauJetContainer* taus);

  /// ensures consistency between channels
  int doLeptonSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons, const xAOD::Electron*& el1,
                        const xAOD::Electron*& el2, const xAOD::Electron*& el3, const xAOD::Muon*& mu1, const xAOD::Muon*& mu2,
                        const xAOD::Muon*& mu3);
  int doLeptonPreSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons, const xAOD::Electron*& el1,
                           const xAOD::Electron*& el2, const xAOD::Electron*& el3, const xAOD::Muon*& mu1, const xAOD::Muon*& mu2,
                           const xAOD::Muon*& mu3);

  virtual bool passLeptonPreSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons,
                                      const xAOD::MissingET* met) = 0;
  virtual bool passLeptonSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons,
                                   const xAOD::MissingET* met) = 0;

  virtual bool passKinematics() = 0;
};

#include "CxAODTools_WZ/WWWEvtSelection.icc"

#endif
