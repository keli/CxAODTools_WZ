#ifndef __EvtWeightVariations_WWW_h__
#define __EvtWeightVariations_WWW_h__

#include <map>
#include <unordered_map>
#include <unordered_set>
#include "xAODEventInfo/EventInfo.h"

class EvtWeightVariations_WWW {
 public:
  EvtWeightVariations_WWW() {
    m_listOfVariations.clear();
    initSherpaWZ();
  };
  ~EvtWeightVariations_WWW(){};

 public:
  float getWeightVariation(const xAOD::EventInfo *eventInfo, const std::string &SYSNAME);
  bool hasVariation(const std::string &SYSNAME);
  bool isSherpaWZ(int DSID);

  std::unordered_set<std::string> getListOfVariations();

 private:
  void initSherpaWZ();
  std::unordered_map<std::string, int> m_variations_SherpaWZ;
  std::unordered_set<std::string> m_listOfVariations;
};

#endif
