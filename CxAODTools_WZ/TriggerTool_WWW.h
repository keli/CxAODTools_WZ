#ifndef CxAODTools_WZ_TriggerTool_WWW_H
#define CxAODTools_WZ_TriggerTool_WWW_H

#include "CxAODTools/TriggerTool.h"

class TriggerTool_WZ : public TriggerTool {
 public:
  TriggerTool_WZ(ConfigStore& config);
  virtual ~TriggerTool_WZ() = default;

  virtual EL::StatusCode initTools() override;
  virtual EL::StatusCode initProperties() override;
  virtual EL::StatusCode initTriggers() override;
 protected:

  virtual void addLowestUnprescaledElectron_WZ();
  virtual void addLowestUnprescaledMuon_WZ();
};

#endif  // ifndef CxAODTools_TriggerTool_WWW_H
